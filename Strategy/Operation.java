package Strategy;

/*
 * Interface Operation
 */
public interface Operation {
	public int doOperation(int number1, int number2);
}
