package Strategy;

/*
 * This class to performs operation Subtract
 */
public class OperationSubtract implements Operation {

	@Override
	public int doOperation(int number1, int number2) {
		return number1 - number2;
	}
}