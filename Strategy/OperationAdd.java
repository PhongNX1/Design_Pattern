package Strategy;

/*
 * This class to performs operation Add
 */
public class OperationAdd implements Operation {

	@Override
	public int doOperation(int number1, int number2) {
		return number1 + number2;
	}
}
