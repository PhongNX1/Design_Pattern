package Strategy;

/*
 * 
 */
public class Context {
	private Operation operation;

	public Context(Operation operationContext) {
		this.operation = operationContext;
	}

	public int executeOperation(int number1, int number2) {
		return operation.doOperation(number1, number2);
	}
}