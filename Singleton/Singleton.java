public class Singleton {

   private static Singleton singleton = new Singleton( );
   
   /* Mot private Constructor de ngan can bat cu 
    * lop nao khac khoi tao.
    */
   private Singleton(){ }
   
   /* Phuong thuc static 'instance' */
   public static Singleton getInstance( ) {
      return singleton;
   }
   /* Cac phuong thuc protected khac */
   protected static void demoMethod( ) {
      System.out.println("demoMethod cho singleton"); 
   }
}