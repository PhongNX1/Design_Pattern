package adapter.composition;

import java.util.List;

/*
 * Interface target: Giao dien khong tuong thich
 */
public interface IShowListName {
	void showListName(List<String> listName);
}