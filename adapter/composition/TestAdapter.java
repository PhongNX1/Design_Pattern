package adapter.composition;

import java.util.ArrayList;

public class TestAdapter {
	public static void main(String[] args) {
		ArrayList<String> array = new ArrayList<>();
		array.add(" Teo ");
		array.add(" Ti ");
		array.add(" Ku ");
		IShowListName adapter = new ShowListNameAdapter(new ShowName());
		adapter.showListName(array);
	}
}
