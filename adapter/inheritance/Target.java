package adapter.inheritance;

/*
 * dinh nghia giao dien Client dang lam viec
 */
public interface Target {
	String estimate(int i);
}
