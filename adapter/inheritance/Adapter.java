package adapter.inheritance;

/*
 * Lop tiep hop, su dung cac phuong thuc ke thua tu lop Adaptee
 */
public class Adapter extends Adaptee implements Target {

	@Override
	public String estimate(int i) {
		return String.valueOf(Math.round(precise(i, 3)));
	}

}
