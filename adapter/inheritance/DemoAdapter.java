package adapter.inheritance;

/*
 * Demo Adapter
 */
public class DemoAdapter {

	public static void main(String[] args) {
		System.out.println("--- Adapter Pattern ---");
		Target target = new Adapter();
		System.out.println(target.estimate(5));
	}
}
