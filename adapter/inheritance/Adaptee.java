package adapter.inheritance;

/*
 * Dinh nghia giao dien lop khong tuong thich
 */
public class Adaptee {
	public double precise(double a, double b) {
		System.out.println("Old lib::precise");
		return a / b;
	}
}